from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import User
from store.models import Store

@login_required
def home(request):

    all_store = Store.objects.filter(owner=request.user)

    if request.user.is_authenticated:
        user = User.objects.filter(email=request.user.email)
        username = user[0].email

    return render(request, 'frontend_home.html', {'username': username, 'all_store': all_store})
