from django.db import models
# from store.models import Store
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from django.core.mail import send_mail

class UserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, store_name, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
            store_name=store_name,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, store_name, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            first_name=first_name,
            last_name=last_name,
            store_name=store_name,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        user.create_store()
        return user

class User(AbstractBaseUser):
    email = models.EmailField(verbose_name='Endereço de Email', max_length=254, unique=True)
    store_name = models.CharField(max_length=256, default='', unique=True, null=True, blank=True)
    first_name = models.CharField(verbose_name='Nome', max_length=254, blank=False, null=True)
    last_name = models.CharField(verbose_name='Sobrenome', max_length=254, blank=False, null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(verbose_name='É Admin', default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'store_name']

    def get_full_name(self):
        # The user is identified by their email address
        seq = (self.first_name, self.last_name)
        return " ".join(seq)
    get_full_name.short_description = 'Nome'

    def get_short_name(self):
        # The user is identified by their email address
        return self.first_name

    def get_store_name(self):
        return self.store_name

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def create_store(self):
        from store.models import Store
        store = Store(owner=self, name=self.store_name)
        store.save()

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)