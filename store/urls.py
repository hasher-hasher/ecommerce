from django.urls import path, include
from .views import store, new_product, products, dashboard, product, delete_product, orders, order, finish_order, edit_order, settings, edit_store_profile, add_category, login, register, edit_intermediate, load_options
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings as set

# The first <int:id> must be the store id
urlpatterns = [
    path('login/', login, name='login'),
    path('register/', register, name='register'),

    path('', store, name='store'),
    path('dashboard/', dashboard, name='dashboard'),
    path('orders/', orders, name='orders'),
    path('order/<int:order_id>', order, name='order'),
    path('edit_order/<int:order_id>', edit_order, name='edit_order'),
    path('edit_intermediate/<int:intermediate_id>', edit_intermediate, name='edit_intermediate'),
    path('finish_order/<int:order_id>', finish_order, name='finish_order'),
    path('add_product/', new_product, name='new_product'),
    path('products/', products, name='products'),
    path('delete_product/<int:product_id>/', delete_product, name='delete_product'),
    path('product/<int:product_id>/', product, name='product'),
    path('settings/', settings, name='settings'),
    path('settings/edit_store_profile', edit_store_profile, name='edit_store_profile'),
    path('add_category/<int:store_id>/', add_category, name='add_category'),

    path('load_options/', load_options, name='load_options'),

    # path('accounts/',admin.site.urls),
] + static(set.MEDIA_URL, document_root=set.MEDIA_ROOT)