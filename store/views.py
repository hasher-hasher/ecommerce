from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, get_object_or_404
from .models import Store, Products, Order, Category, Product_Option, Order_Info, Intermediate_Order_Product
from client.models import User
from .forms import NewStoreForm, Product, EditStoreProfileForm, AddCategory, LoginForm, UserCreationForm, Product_Option_FormSet, EditIntermediateForm
from django.conf import settings as project_settings
from django.http import HttpResponse
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout, authenticate
import json
from collections import OrderedDict
import logging

logging.basicConfig(level= logging.DEBUG, format="[%(levelname)s %(filename)s %(lineno)d] %(message)s")

logger = logging.getLogger()

def login(request):
    form = LoginForm(request.POST)
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        try:
            user = User.objects.get(email=username)
        except:
            raise RuntimeError("User doesn't exist on database")

        if user is not None and user.check_password(password):
            user_authenticate = authenticate(username=username, password=password)
            auth_login(request, user_authenticate)
            return redirect('dashboard')
        else:
            print ('Não é pra logar')

    return render(request, 'login.html', {'form': form})

def register(request):
    form = UserCreationForm(request.POST, None)

    if form.is_valid():
        form.save()
        return redirect('login')

    return render(request, 'register.html', {'form': form})


@login_required()
def store(request):
    # logout(request)
    # print ('print: {}'.format(Store.objects.get(name='Hasher').owner))
    # print ('print: {}'.format(request.user))
    return render(request, 'store.html', {'store': store, 'products': products})

@login_required
def new_product(request):
    form = Product(request.POST, request.FILES, None)

    store = Store.objects.get(owner=request.user)

    if form.is_valid():
        instance = form.save(commit=False)
        instance.store_owner = store
        instance.save()
        return redirect('products')

    return render(request, 'new_product.html', {'form': form, 'store': store})

@login_required
def products(request):
    store = Store.objects.get(owner=request.user)
    products = Products.objects.filter(store_owner=store.id)
    # product = Products.objects.get(id=id)
    return render(request, 'products.html', {'store': store, 'products': products})

@login_required
def product(request, product_id):
    product = get_object_or_404(Products, pk=product_id)
    form = Product(request.POST or None, request.FILES or None, instance=product)

    options = Product_Option.objects.filter(product_owner=product)
    l = []
    for option in options:
        l.append(dict(type=option.type, value=option.value))

    formset_data = l
    formset = Product_Option_FormSet(request.POST or None, queryset=options)

    category_form = AddCategory(request.POST or None)

    store = Store.objects.get(owner=request.user)

    print("Tem option?: ", product.has_option)

    if request.method == 'POST':
        if formset.is_valid():
            print('formset valido')
            instance = formset.save(commit=False)
            for option in instance:
                product.has_option = True
                option.product_owner = product
                option.save()
            for obj in formset.deleted_objects:
                obj.delete()

        else:
            print('formset não valido com erro: ', formset.errors)

        if form.is_valid():
            form.save()
            return redirect('products')
        else:
            return redirect('products')

    return render(request, 'product.html', {'store': store, 'product': product, 'form': form, 'category_form': category_form, 'formset': formset})
# return correct json
def json_format(list1, list2):
    final_dict = OrderedDict()
    for item1, item2 in zip(list1, list2):
        print (item1, item2)
        final_dict[item1] = item2
    print (final_dict)
    return final_dict

@login_required
def delete_product(request, product_id):
    product = get_object_or_404(Products, pk=product_id)
    product.delete()
    return redirect('products', id=id)

@login_required
def dashboard(request):
    store = Store.objects.get(owner=request.user)
    return render(request, 'dashboard.html', {'store': store})

@login_required
def orders(request):
    store = Store.objects.first()
    orders = Order.objects.filter(store_owner=store)
    # intermediate_data_objects = [Intermediate_Order_Product.objects.filter(order=order) for order in orders]

    return render(request, 'orders.html', {'orders': orders})

@login_required
def order(request, order_id):
    store = Store.objects.first()
    intermediate_model = Intermediate_Order_Product.objects.filter(order=Order.objects.get(id=order_id))
    order = Order.objects.get(id=order_id)
    order_info = Order_Info.objects.get(order_owner=order)

    return render(request, 'order.html', locals())

@login_required
def edit_order(request, order_id):
    store = Store.objects.get(owner=request.user)
    order = Order.objects.get(id=order_id)
    form = OrderForm(request.POST or None, request.FILES or None, instance=order)

    if request.method == 'POST':
        if form.is_valid():
            form.save()
        return redirect('orders', id=store.id)

    return render(request, 'edit_order.html', {'form': form, 'store': store, 'order': order})

@login_required
def finish_order(request, order_id):

    order = Order.objects.get(id=order_id)
    order.delivered = True
    order.save()

    return redirect('orders')

@login_required
def discount(request):
    store = Store.objects.get(owner=request.user)
    return render(request, 'discount.html', {'store': store})

@login_required
def settings(request):
    store = Store.objects.get(owner=request.user)
    user = User.objects.get(id=store.owner.id)

    return render(request, 'settings.html', {'store': store, 'user': user})

@login_required
def edit_store_profile(request):
    store = Store.objects.get(owner=request.user)
    form = EditStoreProfileForm(request.POST or None, request.FILES or None, instance=store)

    if form.is_valid():
        form.save()
        return redirect('settings')

    return render(request, 'edit_store_profile.html', {'store': store, 'form': form})

@login_required
def add_category(request, store_id):
    store = Store.objects.get(owner=request.user)
    if request.method == 'POST':
        form = AddCategory(request.POST or None)
        temp_form = form.save(commit=False)
        temp_form.store = store
        if form.is_valid():
            # form.save()
            print ("form: {}".format(temp_form.store))
            temp_form.save()
            print ("Deu certo!")
        else:
            print (form.errors)
            print ('Deu ruim')

    return HttpResponse('')

    
@login_required
def edit_intermediate(request, intermediate_id):
    intermediate = get_object_or_404(Intermediate_Order_Product, id=intermediate_id)
    form = EditIntermediateForm(intermediate.product, request.POST or None, instance=intermediate)

    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('order', order_id=intermediate.order.id)
    # TODO Salvar dados editados
    return render(request, 'edit_intermediate.html', locals())

def load_options(request):
    product_id = request.GET.get('product')
    products_option_qs = Product_Option.objects.filter(product_owner=Products.objects.get(id=product_id))
    print('Got this -> ', products_option_qs)
    return render(request, 'option_dropdown_list_options.html', {'products_option': products_option_qs})