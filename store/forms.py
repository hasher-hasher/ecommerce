from django.forms import ModelForm, Textarea
from django import forms
from .models import Store, Products, Order, Category, Product_Option, Order_Info, Intermediate_Order_Product
from client.models import User
from django.forms.models import modelformset_factory

Product_Option_FormSet = modelformset_factory(Product_Option, exclude=(), can_delete=True)

class NewStoreForm(ModelForm):
    class Meta:
        model = Store
        fields = ['avatar', 'name', 'description']


class Product(ModelForm):
    class Meta:
        model = Products
        fields = ['avatar', 'name', 'description', 'category', 'status', 'price', 'featured', 'options']


class OrderInfoForm(ModelForm):
    class Meta:
        model = Order_Info
        exclude = ['order_owner']
        widgets = {
            'observation_note': Textarea(),
        }


class EditStoreProfileForm(ModelForm):
    class Meta:
        model = Store
        fields = ['avatar', 'name', 'description']


class AddCategory(ModelForm):
    class Meta:
        model = Category
        fields = ['category', 'store']


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email', 'store_name', 'password1', 'password2',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def create_store(self, owner, name):
        store = Store()
        store.owner = owner
        store.name = name
        store.save()
        return store

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
            self.create_store(user, user.store_name)
        return user


class LoginForm(forms.Form):
    username = forms.CharField(max_length=256)
    password = forms.CharField(widget=forms.PasswordInput)


class EditIntermediateForm(ModelForm):
    class Meta:
        model = Intermediate_Order_Product
        fields = ['product', 'product_option', 'product_quantity']

    def __init__(self, product, *args, **kwargs):
        super(EditIntermediateForm, self).__init__(*args, **kwargs)
        self.fields['product_option'].queryset = Product_Option.objects.filter(product_owner=product)

        if 'product' in self.data:
            try:
                product_id = int(self.data.get('product'))
                self.fields['product_option'].queryset = Product_Option.objects.filter(product_owner=product_id).order_by('type')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['product_option'].queryset = self.instance.product.product_option_set.order_by('type')