from django.db import models
from client.models import User
from django.contrib.postgres.fields import JSONField, ArrayField

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'store/{}'.format(filename)

class Store(models.Model):
    owner = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.FileField(upload_to=user_directory_path, default='default.jpg')
    name = models.CharField(max_length=254, default='', unique=True)
    description = models.CharField(max_length=254, null=True, blank=True)

    def __str__(self):
        return self.name

class Category(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE, default=None, blank=True, null=True)
    category = models.CharField(max_length=256)

    def __str__(self):
        return self.category

class Products(models.Model):
    PRODUCT_STATUS = (
        ('ATIVO', 'Ativo'),
        ('INVISIVEL', 'Invisível'),
        ('SEM_ESTOQUE', 'Sem Estoque'),
        ('EM_BREVE', 'Em Breve'),
    )

    store_owner = models.ForeignKey(Store, on_delete=models.CASCADE)
    avatar = models.FileField(upload_to=user_directory_path, default='default.jpg')
    name = models.CharField(max_length=254)
    description = models.CharField(max_length=254, blank=True, null=True)
    status = models.CharField(max_length=254, choices=PRODUCT_STATUS, default='ATIVO')
    price = models.DecimalField(max_digits=7, decimal_places=2, default=0)
    featured = models.BooleanField(default=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=None, null=True, blank=True)
    has_option = models.BooleanField(default=False)
    options = JSONField(blank=True, null=True)

    def __str__(self):
        return self.name

class Product_Option(models.Model):
    product_owner = models.ForeignKey(Products, on_delete=models.CASCADE, null=True, blank=True)
    type = models.CharField(max_length=256, null=True, blank=True)
    value = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.type

class Order(models.Model):
    # Order Info
    active = models.BooleanField(default=False)
    store_owner = models.ForeignKey(Store, on_delete=models.CASCADE)
    order_number = models.CharField(max_length=256)
    product_info = models.ManyToManyField(Products, through='Intermediate_Order_Product')
    order_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    delivered = models.BooleanField(default=False)
    full_price = models.IntegerField(default=0)


    def __str__(self):
        return self.order_number


    def generate_order_number(self):
        first_string = self.store_owner.name.upper()
        first_string = first_string.replace(' ', '')
        second_string = self.id
        return '#{}-{:06d}'.format(first_string, second_string)

    def refresh_full_price(self):
        intermediate = Intermediate_Order_Product.objects.filter(order=self)
        new_full_price = 0
        for intermediate_info in intermediate:
            new_full_price += intermediate_info.product.price * intermediate_info.product_quantity
        self.full_price = new_full_price
        self.save()


    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.order_number = self.generate_order_number()
        super().save(*args, **kwargs)

class Intermediate_Order_Product(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    product_option = models.ForeignKey(Product_Option, on_delete=models.CASCADE, null=True, blank=True)
    product_quantity = models.IntegerField(default=1)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.order.refresh_full_price()
    

class Order_Info(models.Model):
    # Customer Info
    order_owner = models.ForeignKey(Order, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=256, default=None)
    last_name= models.CharField(max_length=256, default=None)
    email = models.EmailField(max_length=256, default=None)
    address = models.CharField(max_length=256, default=None)
    observation_note = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return self.order_owner.order_number

    def get_full_name(self):
        return ('{} {}'.format(self.first_name, self.last_name))

class Cart(models.Model):
    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    product_option = models.ForeignKey(Product_Option, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return self.product