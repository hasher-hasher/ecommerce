from django.contrib import admin
from .models import Store, Order, Products

admin.site.register(Store)

""" class OrderAdmin(admin.ModelAdmin):
    list_display = ('order_number', 'delivered')
    fieldsets = (
        ('Dados', {
            'fields': ('store_owner', 'product', 'delivered')
        }),
    )
admin.site.register(Order, OrderAdmin) """

class ProductsAdmin(admin.ModelAdmin):
    list_display = ('name', 'status', 'price')
    fieldsets = (
        ('Dados', {
            'fields': ('name', 'store_owner', 'status', 'avatar', 'price'),
        }),
    )
admin.site.register(Products, ProductsAdmin)
