from django.urls import path, include
from .views import home
from store import urls as store_urls

urlpatterns = [
    path('', home, name='normal_home'),
    # path('login/', login, name='login'),
    # path('new_user/', register, name='register'),
]