window.sr = ScrollReveal({reset: true});
sr.reveal('.products-reveal');

$('.grid').masonry({
  itemSelector: '.grid-item', // use a separate class for itemSelector, other than .col-
  columnWidth: '.grid-sizer',
  percentPosition: true
});

$("#category-trigger").change(function () {
   console.log("Foi");
});

function category_form_submit () {
    $("#category_form").submit();
}

function add_option () {
    var container = document.createElement("div");
    container.classList.add("form-row");

    var colName = document.createElement("div");
    colName.classList.add("col");

    var inputName = document.createElement("input");
    inputName.setAttribute("type", "text");
    inputName.setAttribute("class", "form-control");
    inputName.setAttribute("placeholder", "Name");
    inputName.setAttribute("name", "options_name");

    var colPrice= document.createElement("div");
    colPrice.classList.add("col");

    var inputPrice= document.createElement("input");
    inputPrice.setAttribute("type", "text");
    inputPrice.setAttribute("class", "form-control");
    inputPrice.setAttribute("placeholder", "Price");
    inputPrice.setAttribute("name", "options_price");

    colName.appendChild(inputName);
    colPrice.appendChild(inputPrice);
    container.appendChild(colName);
    container.appendChild(colPrice);

    document.getElementById("options").appendChild(container);
}