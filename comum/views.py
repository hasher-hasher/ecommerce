from django.shortcuts import render, redirect
from .forms import UserCreationForm, LoginForm
from client.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout
from django_hosts.resolvers import reverse
from django.contrib.auth.decorators import login_required

# def register(request):
#     form = UserCreationForm(request.POST, None)
#
#     if form.is_valid():
#         form.save()
#         return redirect('login')
#
#     return render(request, 'register.html', {'form': form})

# def login(request):
#     form = LoginForm(request.POST)
#     if request.method == 'POST':
#         # logout(request)
#         username = request.POST['username']
#         password = request.POST['password']
#         user = User.objects.get(email=username)
#         if user is not None and user.check_password(password):
#             user_authenticate = authenticate(username=username, password=password)
#             auth_login(request, user_authenticate)
#             return redirect('http://my.localhost:8000')
#         else:
#             print ('Não é pra logar')
#
#     return render(request, 'login.html', {'form': form})

@login_required
def home(request):
    if request.user is not None:
        return redirect('http://my.localhost:8000')
    else:
        return redirect('http://my.localhost:8000/login')