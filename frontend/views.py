from django.shortcuts import render
from django.shortcuts import redirect, get_object_or_404
from store.models import Store, Products, Cart, Product_Option, Order, Intermediate_Order_Product
from store.forms import OrderInfoForm
import json
from collections import OrderedDict
from django.http import HttpResponse

def home(request):
    store = Store.objects.first()
    products = Products.objects.filter(store_owner=store.id)
    return render(request, 'frontend_home.html', {'store': store, 'products': products})

def store_products(request):
    store = Store.objects.first()
    products = Products.objects.filter(store_owner=store).order_by('name')

    return render(request, 'frontend_products.html', {'products': products, 'store': store})

def store_product(request, product_id):
    store = Store.objects.first()
    product = get_object_or_404(Products, pk=product_id)
    product_options = Product_Option.objects.filter(product_owner=product)

    return render(request, 'frontend_product.html', {'store': store, 'product': product, 'product_options': product_options})

def store_add_cart(request, product_id):
    # The variable 'listItem' on the POST return the option selection
    request.session.set_expiry(0)
    product = Products.objects.get(id=product_id)

    l = []

    if 'cart' in request.session:
        l = request.session['cart']
        l.append(dict(product_id=product.id, product_option=request.POST['listItem']))
        request.session['cart'] = l
    else:
        request.session['cart'] = []
        l.append(dict(product_id=product.id, product_option=request.POST['listItem']))
        request.session['cart'] = l

    return redirect('store_cart')

def store_remove_cart(request, product_id):
    product = Products.objects.get(id=product_id)
    l = request.session['cart']

    for x, item in enumerate(l):
        if item['product_id'] == product_id:
            print('deletando item ', x, ' da lista')
            del l[x]

    request.session['cart'] = l

    return redirect('store_cart')


def store_cart(request):
    # del request.session['cart']
    l = []
    
    if 'cart' in request.session:
        session_data = request.session['cart']
        for item in session_data:
            product = Products.objects.get(id=item['product_id'])
            if product.has_option == True:
                product_option = Product_Option.objects.get(product_owner=product, type=item['product_option'])
            else:
                product_option = ''

            if not l:
                l.append(dict(product=product, product_option=product_option, quantity=1))
            elif any(x['product'] == product and x['product_option'] == product_option for x in l):
                idx = [i for i,x in enumerate(l) if x['product'] == product and x['product_option'] == product_option]
                a = l[idx[0]]
                a['quantity'] += 1
                a['total_price'] = a['quantity'] * product.price
                l[idx[0]] = a
            else:
                l.append(dict(product=product, product_option=product_option, quantity=1, total_price=product.price))
                
    print('l final:', l)

    return render(request, 'frontend_cart.html', {'list': l})

def store_get_order_info(request, order_id):
    form = OrderInfoForm(request.POST or None)
    order_id = order_id

    if request.method == 'POST':
        if form.is_valid():
            instance = form.save(commit=False)
            instance.order_owner = Order.objects.get(id=order_id)
            instance.save()
            order = Order.objects.get(id=order_id)
            order.active = True
            order.save()
            print('salvou')
            return redirect('success_order')
        else:
            print('Problema: ', form.errors)

    return render(request, 'frontend_order_data.html', {'form': form, 'order_id': order_id})

def store_create_order(request):
    [x.delete() for x in Intermediate_Order_Product.objects.all()]
    print('-> ', request.POST)
    if request.method == 'POST':
        order = Order()
        order.store_owner = Store.objects.first()
        print(sum(map(float, request.POST.getlist('product_final_price'))))
        order.full_price = sum(map(float, request.POST.getlist('product_final_price')))
        order.save()
        for idx, product_id in enumerate(request.POST.getlist('product_id')):
            product = get_object_or_404(Products, id=product_id)
            if product.has_option:
                product_option = get_object_or_404(Product_Option, type=request.POST.getlist('product_option')[idx])
            product_quantity = request.POST.getlist('product_quantity')[idx]
            if 'product_option' in locals():
                relation = Intermediate_Order_Product(
                    order=order,
                    product=product,
                    product_option=product_option,
                    product_quantity=product_quantity
                )
            else:
                relation = Intermediate_Order_Product(
                    order=order,
                    product=product,
                    product_quantity=product_quantity
                )
            relation.save()
    return redirect('store_get_order_info', order_id=order.id)

def success_order(request):
    return render(request, 'frontend_success_order.html')