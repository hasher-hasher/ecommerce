from django.urls import path, include
from .views import home, store_products, store_product, store_cart, store_add_cart, store_remove_cart, store_create_order, store_get_order_info, success_order
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    # path('', include('registration.backends.default.urls')),
    path('', home, name='home'),
    path('store_products/', store_products, name='store_products'),
    path('store_product/<int:product_id>', store_product, name='store_product'),
    path('store_cart/', store_cart, name='store_cart'),
    path('store_add_cart/<int:product_id>/', store_add_cart, name='store_add_cart'),
    path('store_remove_cart/<int:product_id>/', store_remove_cart, name='store_remove_cart'),
    path('store_create_order/', store_create_order, name='store_create_order'),
    path('store_get_order_info/<int:order_id>', store_get_order_info, name='store_get_order_info'),
    path('success_order', success_order, name='success_order'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)