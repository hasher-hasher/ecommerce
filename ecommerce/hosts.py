from django.conf import settings
from django_hosts import patterns, host

host_patterns = patterns('',
    host(r'www', 'ecommerce.urls', name='www'),
    host(r'my', 'store.urls', name='my'),
    host(r'store', 'frontend.urls', name='store'),
)